<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;
use App\Category;
use Auth;
use Carbon\Carbon;
use App\Transection;
class PageController extends Controller
{
	public function __construct()
	{
		$slide=Slide::all();
		view()->share('slide',$slide);
	}
    public function index()
    {
    	$cat=Category::all();
    	$slide=Slide::all();
    	return view('users.index',compact('slide','cat'));

    }
    public function detail()
    {	
    	if(Auth::check()) 	
    	{
    		$id=Auth::user()->id;
    	}
    	$tran=Transection::where('users_id','=',$id)->get();
    	$cat=Category::all();
    	return view('users.detail',compact('tran','cat'));
    }
    public function intro()
    {
    	return view('users.intro');
    }
    public function gop_y()
    {
    	return view('users.gopy');
    }
    public function contact()
    {
    	return view('users.lienhe');
    }
    public function view_detail()
    {
    	return view('users.viewDetail');
    }
    public function edit_tran(Request $req,$id)
    {   

        $tran=Transection::find($id);
        $tran->name=$req->name;
        $tran->money=$req->money;
        $tran->money_type=$req->money_type;
        $tran->note=$req->note;
        $tran->save();
        return redirect()->back()->with('mes','Ban da sua thanh cong');
    }
    public function del_tran($id)
    {
        $tran=Transection::find($id);
        $tran->delete();
        return redirect()->back()->with('mes1','Ban da xoa thanh cong');
    }
    public function add_tran(Request $req,$id)
    {
       
        $now = Carbon::now();
        $tran=new Transection;
        if(Auth::check())   
        {
            $id1=Auth::user()->id;
        }
        $tran->users_id=$id1;
        $tran->category_id=$id;
        $tran->name=$req->name;
        $tran->money=$req->money;
        $tran->money_type=$req->money_type;
        $tran->note=$req->note;
        $tran->save();
        return redirect()->back()->with('addtr','Ban da them thanh cong');
        
    }
}

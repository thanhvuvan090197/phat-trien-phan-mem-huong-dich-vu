<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Transection;
use Auth;

class ajaxController extends Controller
{

    public function get_detail($id)
    {
    	if(Auth::check())
    	{
    		$user_id=Auth::user()->id;
    	}
    	$tran=Transection::where('category_id','=',$id,'and')->where('users_id','=',$user_id)->get();
    	if(count($tran)>0)
    	{

	    	echo " <tr class='text-center'>
	                                        <th>STT</th>
	                                        <th>Khoản chi</th>
	                                        <th>Số tiền</th>
	                                        <th>DV Tien</th>
	                                        <th>Thao tác</th>
	                                    </tr>";
	    	foreach($tran as $tr)
	    	{
	    							echo    "<tr style='margin-top:50px'>
	    									<td>1</td>
	                                        <td>".$tr->name."</td>
	                                        <td>".$tr->money."</td>
	                                        <td>".$tr->money_type."</td>
	                                        <td>
	                                        <i onclick='load_data1(".$tr->id.")' class='btn btn-primary fa fa-eye'></i> 
											
	                                        <i onclick='edit(".$tr->id.")' class='btn btn-primary fa fa-edit'></i>
	                                        <i onclick='del1(".$tr->id.")' class='btn btn-danger fa fa-remove delete' title='remove'></i>

	                                        </td>
	                                        </tr>";

	                                        
	    	}
    	}
    	else{
    		echo '<h3  style="text-align: center;color: red">Khong co chi tieu nao</h3>';
    	}
    	

    }
    public function show_detail($b)
    {
    	$tran=Transection::where('id','=',$b)->get();
    	echo '	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h4 class="text-center" style="font-weight: bold;color: blue">Danh sách chi tiêu </h4>
				<table class="table table-hover">
					<h3 class="alert alert-info"><em>Tháng 9</em> <a href=""><span style="float: right;font-size: 15px" class="fa fa-long-arrow-right">Tháng 8</span></a></h3>
					<thead>
						<tr>
							<th style="padding-left:300px">STT</th>
							<th>TÊN CHI TIÊU</th>
							<th>SỐ TIỀN</th>
							<th>GHI CHÚ</th>
							<th>Thời gian</th>
						</tr>
					</thead>
					<tbody>';
				 foreach($tran as $tr){
				 	echo '
						<tr>
							<td >1</th>
							<td>'.$tr->name.'</td>
							<td>'.$tr->money.'</td>
							<td>'.$tr->note.'</td>
							<td>'.$tr->created_at.'</td>
						</tr>
						 <hr style="width:100%">';
					}
						
					echo '</tbody>
				</table>
				<nav aria-label="Page navigation">
				  <ul class="pagination">
				    <li>
				      <a href="#" aria-label="Previous">
				        <span aria-hidden="true">&laquo;</span>
				      </a>
				    </li>
				    <li><a href="#">1</a></li>
				    <li><a href="#">2</a></li>
				    <li><a href="#">3</a></li>
				    <li><a href="#">4</a></li>
				    <li><a href="#">5</a></li>
				    <li>
				      <a href="#" aria-label="Next">
				        <span aria-hidden="true">&raquo;</span>
				      </a>
				    </li>
				  </ul>
				</nav>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-10"></div>
			<div class="col-sm-2">
				<a href="" class="btn btn-danger">Export to excel</a>
			</div>
		</div>
	</div>';	
    }
    public function edit($id)
    {
    	$a=$id;
    	$tran=Transection::where('id','=',$id)->get();

    	
	    foreach($tran as $tr)
	    {

    		
	    		echo '					
	                <div class="container text-center" id="123">

					<form  id="edit_tran" method="post" action="'.route("edit_tran",$a).'">
					    '.csrf_field().'
					    <div class="group">      
					      <input type="text" name="name" required>
					      <span class="highlight"></span>
					      <span class="bar"></span>
					      <label><i class="fa fa-signature">&nbsp &nbsp Ten chi Tieu&nbsp(&nbsp'.$tr->name.')</i></label>
					    </div>
					      
					    <div class="group">      
					      <input type="text" name="money" required>
					      <span class="highlight"></span>
					      <span class="bar"></span>
					      <label><i class="fa fa-dollar-sign">&nbsp &nbsp So tien&nbsp(&nbsp'.$tr->money.')</i></label>
					      

					    </div>

					     <div class="group">      
					      <input type="text" name="money_type" required>
					      <span class="highlight"></span>
					      <span class="bar"></span>
					      <label><i class="fas fa-money-check-alt">&nbsp &nbsp Loai tien(&nbsp '.$tr->money_type.')</i></label>
					    </div>

					     <div class="group">      
					      <input type="text" name="note" required>
					      <span class="highlight"></span>
					      <span class="bar"></span>
					      <label><i class="fas fa-pencil-alt">&nbsp  &nbsp Ghi chu&nbsp(&nbsp'.$tr->note.')</i></label>
					    </div>
					  </form>
					</div>'
					;
	   	}

    }
    public function add_tran($id)
    {

	    		echo '				
	                <div class="container " id="123">
  
					<a href="" onclick="close()"></a>
					  
					<form id="add_tran" method="post" action="'.route("add-tran",$id).'" >
					    '.csrf_field().'
					    <div class="group">      
						    <input type="text" name="name" required>
						    <span class="highlight"></span>
						    <span class="bar"></span>
						      <label><i class="fa fa-signature">&nbsp &nbsp Ten chi Tieu&nbsp&nbsp</i></label>
					    </div>
					      
					    <div class="group">      
					      <input type="text" name="money" required>
					      <span class="highlight"></span>
					      <span class="bar"></span>
					      <label><i class="fa fa-dollar-sign">&nbsp &nbsp So tien&nbsp&nbsp</i></label>

					    </div>

					     <div class="group">      
					      <input type="text" name="money_type" required>
					      <span class="highlight"></span>
					      <span class="bar"></span>
					      <label><i class="fas fa-money-check-alt">&nbsp &nbsp Loai tien&nbsp </i></label>
					    </div>

					     <div class="group">      
					      <input type="text" name="note" required>
					      <span class="highlight"></span>
					      <span class="bar"></span>
					      <label><i class="fas fa-pencil-alt">&nbsp  &nbsp Ghi chu&nbsp&nbsp</i></label>
					    </div>
					  </form>
					</div>'
					;
    }
    public function editheader()
    {
    	echo ' <i class="fas fa-times" style="color:white;padding-left:150px"  onclick="dlgLogin()"></i>&nbsp &nbspEdit Transection  &nbsp ';
    }
    public function view_detail()
    {
    	echo ' <i class="fas fa-times" style="color:white;padding-left:150px"  onclick="dlgLogin()"></i>&nbsp &nbspTransection List  &nbsp ';
    }
    Public function ajax_add()
    {
    	echo ' <i class="fas fa-times" style="color:white;padding-left:150px"  onclick="dlgLogin()"></i>&nbsp &nbsp Add  Transection &nbsp ';
    }
    public function ajaxdata($id)

    {

    	echo '	<button  onclick="add('.$id.')" class="btn btn-danger">&nbsp Add &nbsp</button>
             	<button class="btn btn-danger" onclick="dlgLogin()">Cancel</button>';
    }
    public function ajaxdata1()
    {
    	echo '	<button class="btn btn-danger"  onclick="abc()">&nbsp Done  &nbsp </button>
             	<button class="btn btn-danger" onclick="dlgLogin()">Cancel</button>';
    }
    public function ajaxdata2($id)
    {

    	echo '	<button class="btn btn-danger"  onclick="addtran('.$id.')">&nbsp Done  &nbsp </button>
             	<button class="btn btn-danger" onclick="dlgLogin()">Cancel</button>';
    }
    public function del_succ()
    {
    	echo 'Ban da xoa thanh cong';
    }
}

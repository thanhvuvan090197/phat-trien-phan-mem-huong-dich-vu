<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentModel extends Model
{
    protected $table = 'document';

    public function addDocument($name,$content,$path,$created_by){
        $this->name = $name;
        $this->content = $content;
        $this->file = $path;
        $this->created_by = $created_by;
        if($this->save()){
            return true;
        }else{
            return false;
        }
    }
}

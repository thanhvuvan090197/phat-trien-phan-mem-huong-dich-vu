@extends('admin.master')
@section('content')
    <div class="row">
        <div class="col-xs-2"></div>
        <div class="col-xs-8">
            <h2 class="text-center">Thêm tài liệu hệ thống</h2>
                <form method="post" action="{!! route('setAddDocument') !!}" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                          <label for="exampleInputEmail1">Tên tài liệu</label>
                          <input name="name" type="text" class="form-control" id="exampleInputEmail1" placeholder="Tên tài liệu">
                        </div>
                        <div class="form-group">
                            <label for="">Nội dung</label>
                                <textarea class="form-control ckeditor" rows="3" name="contentDocument" id="ckeditor"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Tệp đính kèm</label>
                            <input type="file" name="uploadDocument">
                        </div>
                        <div class="checkbox">
                          <label>
                            <input type="checkbox"> Public ?
                          </label>
                        </div>
                        <div class="form-group">
                          <div class="g-recaptcha" data-sitekey="6LfRAHIUAAAAAD6Eubf5DwZzw8VLsZCGI8MLIptb"></div>
                        </div>
                        <button type="submit" class="btn btn-default">Thêm tài liệu</button>
                      </form>
        </div>
        <div class="col-xs-2"></div>
    </div>
@endsection
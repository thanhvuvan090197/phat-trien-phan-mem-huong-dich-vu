@extends('admin.master')
@section('content')
<div class="row">
        <div class="col-xs-2"></div>
        <div class="col-xs-8">
            <h2 class="text-center">Sửa thông tin thành viên</h2>
                <form method="post" action="{!! route('setAddUser') !!}" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                          <label for="exampleInputEmail1">Tên đăng nhập</label>
                          <input type="text" class="form-control" id="exampleInputEmail1" placeholder="UserName" name="name">
                        </div>
                        <div class="form-group">
                                <label for="exampleInputEmail1">Mật khẩu</label>
                                <input type="password" class="form-control" id="exampleInputEmail1" placeholder="Password" name="password">
                        </div>
                        <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" name="email">
                        </div>
                        <div class="form-group">
                          <label for="exampleInputPassword1">Level</label>
                          <select class="form-control" name="level">
                                <option>1</option>
                                <option>2</option>
                              </select>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputFile">Ảnh đại diện</label>
                          <input type="file" id="exampleInputFile" name="images_avatar">
                          
                        </div>

                        <div class="form-group">
                            <label for="">Mô tả ngắn</label>
                                <textarea class="form-control ckeditor" rows="3" name="content_users" id="ckeditor"></textarea>
                        </div>
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" name="active"> Active ?
                          </label>
                        </div>
                        <div class="form-group">
                          <div class="g-recaptcha" data-sitekey="6LfRAHIUAAAAAD6Eubf5DwZzw8VLsZCGI8MLIptb"></div>
                        </div>
                        <button type="submit" class="btn btn-default">Thêm thành viên</button>
                      </form>
        </div>
        <div class="col-xs-2"></div>
    </div>
@endsection
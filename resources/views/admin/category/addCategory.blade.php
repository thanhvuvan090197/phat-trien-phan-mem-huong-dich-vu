@extends('admin.master')
@section('content')
<?php
  
  function createMenu($array,$space = '',$id_parent = 0,$loop = 0){
    if(!empty($array)){
      foreach($array as $k=>$v){
        if($v->category_id == $id_parent){
          echo '<option value="'.$v->id.'">' . $space . $v->name . '</option>';
          createMenu($array,$space.'--|',$v->id);
        }  
    }
    }
    
  }
 ?>
    <div class="row">
        <div class="col-xs-2"></div>
        <div class="col-xs-8">
            <h2 class="text-center">Thêm chuyên mục</h2>
                <form action="{!! route('setAddCategory')!!}" method="post" enctype="multipart/form-data">
                  @csrf
                        <div class="form-group">
                          <label for="exampleInputEmail1">Tên chuyên mục</label>
                          <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Tên chuyên mục">
                        </div>
                        <div class="form-group">
                          <label for="exampleInputPassword1">Chuyên mục cha</label>
                          <select class="form-control" name="category_id">
                                <?php  createMenu($dataCategory); ?>
                              </select>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputFile">Ảnh chuyên mục</label>
                          <input type="file" id="exampleInputFile" name="imagesCategory">
                          
                        </div>

                        <div class="form-group">
                            <label for="">Mô tả</label>
                                <textarea class="form-control ckeditor" rows="3" name="comment" id="ckeditor"></textarea>
                        </div>
                        <div class="form-group">
                          <div class="g-recaptcha" data-sitekey="6LfRAHIUAAAAAD6Eubf5DwZzw8VLsZCGI8MLIptb"></div>
                        </div>
                        <button type="submit" class="btn btn-default">Thêm chuyên mục</button>
                      </form>
        </div>
        <div class="col-xs-2"></div>
    </div>
@endsection
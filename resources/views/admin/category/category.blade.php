@extends('admin.master')
@section('content')

<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Danh sách categories</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
              <table id="example1" class="table table-bordered table-striped">
                <thead class="text-center">
                <tr>
                  <th>ID</th>
                  <th>CATEGORY NAME</th>
                  <th>IMAGES</th>
                  <th>ACTIONS</th>
                </tr>
                </thead>
                <tbody>
                
                  @foreach($dataCategory as $k=>$v)
                   <tr>
                    <td>{!! $v->id !!}</td>
                    <td>{!! $v->name !!}</td>
                    <td>{!! asset('storage/app') !!}<?php echo '/' . $v->images ?></td>
                    <td>
                      <i class="btn btn-success fa fa-edit"></i>
                      <i class="btn btn-danger fa fa-remove"></i>
                    </td>
                  </tr>
                  @endforeach

                </tbody>
              </table>
              <div class="pagi text-center">
                 <nav aria-label="Page navigation">
                  <ul class="pagination">
                      <li>
                          <a href="#" aria-label="Previous">
                              <span aria-hidden="true">&laquo;</span>
                          </a>
                      </li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">5</a></li>
                      <li>
                          <a href="#" aria-label="Next">
                              <span aria-hidden="true">&raquo;</span>
                          </a>
                      </li>
                  </ul>
              </nav>
              </div>
             
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
@endsection
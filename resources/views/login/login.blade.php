@extends('login.master')
@section('login')
    <form action="{!! route('setLogin') !!}" name="formLogin" method="post">
    	@csrf
    	
        <input type="text" name="username" placeholder="username">
        <input type="password" name="password" placeholder="password">
        <input type="submit" name="submit" value="Login">
        <a href="{{ route('facebook.login')}}"><input type="button" name="submit" value="Facebook Login"></a>
        <p style="text-align: center;margin-top: 15px;"><a href="{!! route('register') !!}">Register member ?</a></p>
    </form>
@endsection
                 
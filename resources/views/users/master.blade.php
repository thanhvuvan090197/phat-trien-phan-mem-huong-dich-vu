<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{!! asset('lib/bootstrap/css/bootstrap.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('lib/font-awesome/css/font-awesome.min.css') !!}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

   <!-- Start WOWSlider.com HEAD section -->
    <link rel="stylesheet" type="text/css" href="{!!asset('engine1/style.css')!!}" />
    <script type="text/javascript" src="{!!asset('engine1/jquery.js')!!}"></script>
    <!-- End WOWSlider.com HEAD section -->
    <link type="text/css" rel="stylesheet" href="{!! asset('style.css') !!}">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

</head>
<body>
    <style>
            #white-background{
                display: none;
                width: 100%;
                height: 100%;
                position: fixed;
                top: 0px;
                left: 0px;
                background-color: #fefefe;
                opacity: 0.7;
                z-index: 9999;
            }
            
            #dlgbox{
                /*initially dialog box is hidden*/
                display: none;
                position: fixed;
                width: 680px;
                z-index: 9999;
                border-radius: 10px;
                background-color: #7c7d7e;
            }
            
            #dlg-header{
                background-color: #6d84b4;
                color: white;
                font-size: 20px;
                padding: 10px;
                margin: 10px 10px 0px 10px;
            }
            
            #dlg-body{
                background-color: white;
                color: black;
                font-size: 14px;
                padding: 10px;
                margin: 0px 10px 0px 10px;
            }
            
            #dlg-footer{
                background-color: #f2f2f2;
                text-align: right;
                padding: 10px;
                margin: 0px 10px 10px 10px;
            }
            
            #dlg-footer button{
                background-color: #6d84b4;
                color: white;
                padding: 5px;
                border: 0px;
            }
            th{
                text-align: left;
                padding-right: 50px;
            }
            tr{
                padding-bottom: 40px;
            }
        </style>

<div class="wrapper">
    <div class="logo">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <a href="{!! route('users/index') !!}"><img src="{!! asset('images/logo2.png') !!}" alt="" ></a>
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-4 head-logo">
                    <div class="content">
                        <span>Currency</span>
                        <select name="" id="">
                            <option value="">US Dollar $</option>
                            <option value="">VNĐ</option>
                            <input type="submit" value="Change">
                        </select>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="menu">
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="{!! route('users/index') !!}">Trang chủ <span class="sr-only">(current)</span></a></li>
                        <li><a href="{!! route('users/intro') !!}">Giới thiệu</a></li>
                        <li><a href="{!! route('users/gop-y') !!}">Góp ý</a></li>
                        <li><a href="{!! route('users/lien-he') !!}">Liên hệ</a></li>
                    </ul>
                    <form class="navbar-form navbar-left">
                        <div class="form-group">
                            <input type="text" class="form-control input-search" placeholder="Search">
                        </div>
                        <button type="button" class="btn btn-default fa fa-search" id="search-btn"></button>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                @if(Auth::check())
                                {{ Auth::user()->name}}
                                

                             <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="">Setting</a></li>
                                <li><a href="{!! route('logout') !!}">Logout</a></li>
                            </ul>
                            @else
                            <ul style="">
                                <li style=""><a href="{{ route('register') }}">Register</a>
                                <a href="{{ route('login') }}">Login</li>
                            </ul>
                            @endif
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

    </div> <!--end-menu-->
    <div class="slide" style="height: 400px;overflow: hidden;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <!-- Start WOWSlider.com BODY section -->
                    <div id="wowslider-container1">
                    <div class="ws_images"><ul>
                        @foreach($slide as $sl)
                            <li><img src="data1/images/<?php echo $sl->link ?>" alt="" title="" id="wows1_0"/></li>
                        @endforeach
                        </ul></div>
                        <div class="ws_bullets"><div>
                            @foreach($slide as $sl)
                            <a href="#" title=""><span><img src="data1/tooltips/<?php echo $sl->link ?>" alt=""/>1</span></a>
                            @endforeach
                        </div></div><div class="ws_script" style="position:absolute;left:-99%"><a href="http://wowslider.net">jquery image slider</a> by WOWSlider.com v8.8</div>
                    <div class="ws_shadow"></div>
                    </div>  
                    <script type="text/javascript" src="{!!asset('engine1/wowslider.js')!!}"></script>
                    <script type="text/javascript" src="{!!asset('engine1/script.js')!!}"></script>
                    <!-- End WOWSlider.com BODY section -->
                </div>
            </div>
        </div>
    </div> <!--end-slide-->
    @yield('user')
    <div class="footer">
        <div class="container text-center">
            <div class="row">
                <div class="col-sm-4 footer-copyright">
                    <p>Bản quyền thuộc về KTPM1-K10 <br> &copy 2016-2018</p>
                </div>
                <div class="col-sm-4">
                    <ul class="menu-footer">
                        <li><a href="{!! route('users/index') !!}">Trang chủ</a></li>
                        <li><a href="{!! route('users/intro')!!}">Giới thiệu</a></li>
                        <li><a href="{!! route('users/gop-y') !!}">Góp ý</a></li>
                        <li><a href="{!! route('users/lien-he') !!}">Liên hệ</a></li>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <p>
                        <i class="fa fa-facebook fb"></i>
                        <i class="fa fa-youtube yt"></i>
                        <i class="fa fa-linkedin lk"></i>
                    </p>
                </div>
            </div>
        </div>
    </div> <!--end-footer-->
</div><!-- end-wrapper-->
@yield('script')


<script src="{!! asset('lib/bootstrap/js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('admin/ckeditor/ckeditor.js') !!}"></script>
</body>
</html>
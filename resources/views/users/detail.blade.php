@extends('users.master')
@section('user')

        <div class="showDetail" id="show">
            
            <div class="container-fuild showDetail-one">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="menuShowDetail text-left">
                            <h4 class="alert alert-success" style="text-align: center"><em>Saving Money</em></h4>
                            <ul id="type">
                               @foreach($cat as $ca)
                                <li class="" onclick="load_data(<?php echo $ca->id ?>)" id="<?php echo $ca->id ?>" value="<?php echo $ca->id ?>" >
                                    <?php echo $ca->name ?>
                                </li>
                                <h3 style="text-align: center;color: red">Khong co chi tieu nao</h3>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-9 contentParent">
                        <div class="contentShowDetail ">
                            <div class="oneBlockShowDetail familyContent ">
                                <h4 style="text-align: center;" class="alert alert-success ">Family|Today <a href="{!! route('users/view-detail') !!}"><span class="btn btn-danger text-right">View detail</span></h4></a>
                                <hr>
                                <table id="result">

                                   
                                </table>

                            </div>
                        </div>
                        
                    </div>
                    
                </div>
                  <div>
                
           </div>

            </div>
         

        </div>
        
       @endsection

       @section('script')
           <script type="text/javascript">
              
                   function load_data(a){
           
                       $.get('ajax/'+a,function(data){
                           $('#result').html(data);
                       });
                   }
                   function load_data1(b){
                       $.get('ajax2/'+b,function(data){
                           $('#show').html(data);
                       });
                   }
                   function add_detail(){
                       $.get('ajax3',function(data){
                           $('#show').html(data);
                       });
           
                   }
                    $(document).ready(function() {
                        $(function() {
                        $( "#my_dialog" ).dialog({
                            autoOpen: false
                            });
                        });
                        $("#b1").click(function(){
                            $( "#my_dialog" ).dialog( "open" );
                        })
                    })
            </script>
             
                    
                                                   
       @endsection
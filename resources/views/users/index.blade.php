@extends('users.master')
@section('user')
@include('users.style')
<div>
    @if(Session::has('mes'))
    <div class="alert alert-success">
        {{ Session::get('mes')}}
    </div>
    @endif
</div>
        <div class="manyBlock">
            <div class="container oneBlock">
                <div class="row">
                    @foreach($cat as $ca)
                    <div class="col-sm-4 col-md-4  text-center showAll">
                        <div class="thumbnail">
                            <a href="{!! route('detail') !!}"><img src="images/<?php echo $ca->images ?>"  alt="Family Images" height="320px"></a>
                            <div class="caption">
                                 <h3 id="<?php echo $ca->id ?>" onclick="csdl(<?php echo $ca->id ?>)" class="btn btn-primany" value='<?php echo $ca->id ?>' ><?php echo $ca->name  ?></h3> 
                            </div>
                        </div>
                        <div class="oneBlock-one">
                            <p>Tổng tiền: 5.000.000 VNĐ</p>
                            <div class="control">
                                {{-- <span title="View"><a href="{!! route('detail'); !!}"><i class="fa fa-eye" aria-hidden="true"></i></a></span> --}}
                            </div>
                        </div>
                    </div>
                    @endforeach
                    
                </div>
            </div>
            
                <div id="my_dialog" >
                    <div class="col-sm-9 contentParent">
                        <div class="contentShowDetail ">
                            <div class="oneBlockShowDetail familyContent">
                                <h4  class="alert alert-success  text-center"><i class="fa fa-edit">Transection Infomation </i> <a href="{!! route('users/view-detail') !!}"></h4></a>
                                <hr>
                                <table id="result" style="width: 600px">
                                
                                   
                                </table>


                            </div>
                        </div>
                        
                    </div>
                </div>
                <div id="test">
                        <div id="white-background">
                        </div>
                        <div id="dlgbox">
                            <div id="dlg-header">Thong bao</div>
                            <div id="dlg-body">Ban da them thanh cong</div>
                            <div id="dlg-footer">
                                 
                                    <button class="btn btn-danger" onclick="dlgLogin()">Cancel</button>

                            </div>
                        </div>
        
        <!-- rest of the page -->
        
                </div>
                <div id="del"></div>
            
        </div> <!-- end-manyBlock -->
    @endsection
    @section('script')
    <script type="text/javascript">
              
                   function load_data(a){
           
                       $.get('ajax/'+a,function(data){
                           $('#result').html(data);
                       });
                   }
                   function edit(b){
                        var node=0;
                        var id=b;
                            $('#dlg-body').load("ajax2/"+id);
                            $('#dlg-header').load("ajax21");
                            $('#dlg-footer').load("ajaxdata1");
                            var whitebg = document.getElementById("white-background");
                            var dlg = document.getElementById("dlgbox");
                            whitebg.style.display = "block";
                            dlg.style.display = "block";
                            
                            var winWidth = window.innerWidth;
                            var winHeight = window.innerHeight;
                             dlg.style.left = (winWidth/2) - 480/2 + "px";
                             dlg.style.top = "100px";
                             

                            
                   }
                   function add_detail(){
                       $.get('ajax3',function(data){
                           $('#show').html(data);
                       });
           
                   }
                   function csdl(a){
                            
                            var id=a;
                            
                            $('#dlg-body').load("ajax/"+id);
                            $('#dlg-header').load("ajax1");
                            $('#dlg-footer').load("ajaxdata/"+id);
                            var whitebg = document.getElementById("white-background");
                            var dlg = document.getElementById("dlgbox");
                            whitebg.style.display = "block";
                            dlg.style.display = "block";
                            
                            var winWidth = window.innerWidth;
                            var winHeight = window.innerHeight;
                             dlg.style.left = (winWidth/2) - 480/2 + "px";
                            dlg.style.top = "150px";
                            
                            /*$( "#my_dialog" ).dialog( "open" );*/
                        }
                    $(document).ready(function() {
                         

                        $(function () {
                            
                            $( "#my_dialog" ).dialog({
                                autoOpen: false,
                                width:650,
                                height:500,
                                show: {
                                    effect: "blind",
                                    duration: 1000
                                },
                                hide: {
                                    effect: "blind",
                                    duration: 1000
                                },
                                buttons: {
                                    "Close ": function() {
                                      $( this ).dialog( "close" );
                                    },
                                }
                                
                                });

                        });
                        
                    })

            function dlgLogin(){
                var whitebg = document.getElementById("white-background");
                var dlg = document.getElementById("dlgbox");
                whitebg.style.display = "none";
                dlg.style.display = "none";
            }
            
            function showDialog(){
                var whitebg = document.getElementById("white-background");
                var dlg = document.getElementById("dlgbox");
                whitebg.style.display = "block";
                dlg.style.display = "block";
                
                var winWidth = window.innerWidth;
                var winHeight = window.innerHeight;
                
                dlg.style.left = (winWidth/2) - 480/2 + "px";
                dlg.style.top = "150px";
            }
            
            function abc(){
                document.getElementById("edit_tran").submit();

            }  
            function del1(e){
                $.get('del-tran/'+e,function(data){
                           $('#del').html(data);
                       });

            }
            function addtran(g)
            {
                document.getElementById("add_tran").submit();
            }
            function add(a)
            {
                var e=a;

                $('#dlg-body').load("add-tran/"+e);
                            $('#dlg-header').load("ajax-add");
                            $('#dlg-footer').load("ajaxdata2/"+e);
                            var whitebg = document.getElementById("white-background");
                            var dlg = document.getElementById("dlgbox");
                            whitebg.style.display = "block";
                            dlg.style.display = "block";
                            
                            var winWidth = window.innerWidth;
                            var winHeight = window.innerHeight;
                             dlg.style.left = (winWidth/2) - 480/2 + "px";
                             dlg.style.top = "100px";
            }
            var msg = '{{Session::get('addtr')}}';
            var exist = '{{Session::has('addtr')}}';
              if(exist){
                     
                            var whitebg = document.getElementById("white-background");
                            var dlg = document.getElementById("dlgbox");
                            whitebg.style.display = "block";
                            dlg.style.display = "block";
                            
                            var winWidth = window.innerWidth;
                            var winHeight = window.innerHeight;
                             dlg.style.left = (winWidth/2) - 480/2 + "px";
                            dlg.style.top = "150px";
                            
              }

            var msg = '{{Session::get('mes1')}}';
            var exist = '{{Session::has('mes1')}}';
              if(exist){
                            $('#dlg-body').load("del-succ");
                            var whitebg = document.getElementById("white-background");
                            var dlg = document.getElementById("dlgbox");
                            whitebg.style.display = "block";
                            dlg.style.display = "block";
                            
                            var winWidth = window.innerWidth;
                            var winHeight = window.innerHeight;
                             dlg.style.left = (winWidth/2) - 480/2 + "px";
                            dlg.style.top = "150px";
                            
              }



            </script>
    @endsection